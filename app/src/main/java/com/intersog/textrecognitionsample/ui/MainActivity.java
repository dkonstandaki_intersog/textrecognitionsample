package com.intersog.textrecognitionsample.ui;

import android.content.Intent;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.intersog.textrecognitionsample.R;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private static final int REQUEST_CODE_CAPTURE = 777;

    private TextView tvRecognisedText;

    private TextToSpeech textToSpeech;

    private String currentTextValue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvRecognisedText = findViewById(R.id.tvRecognisedText);

        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.SUCCESS) {
                    showToast(R.string.tts_init_failed);
                }
            }
        });

        findViewById(R.id.btnRecognise).setOnClickListener(this);
        findViewById(R.id.btnSpeak).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.btnRecognise:
                Intent intent = new Intent(this, CaptureActivity.class);
                startActivityForResult(intent, REQUEST_CODE_CAPTURE);
                break;
            case R.id.btnSpeak:
                speak();
                break;
        }
    }

    private void speak() {
        if (TextUtils.isEmpty(currentTextValue)) {
            showToast(R.string.tts_no_text);
            return;
        }

        int speechStatus = textToSpeech.speak(currentTextValue, TextToSpeech.QUEUE_FLUSH, null);
        if (speechStatus == TextToSpeech.ERROR) {
            showToast(R.string.tts_error);
        }
    }

    private void showToast(int msgResId) {
        Toast toast = Toast.makeText(getApplicationContext(), msgResId, Toast.LENGTH_SHORT);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CAPTURE) {
            View root = findViewById(R.id.clRoot);
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    currentTextValue = data.getStringExtra(CaptureActivity.INTENT_EXTRA_TEXT);
                    tvRecognisedText.setText(currentTextValue);
                    Snackbar.make(root, R.string.success, Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(root, R.string.fail, Snackbar.LENGTH_SHORT).show();
                }
            } else {
                Snackbar.make(root, R.string.error, Snackbar.LENGTH_SHORT).show();
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
